# React tests example

This repo uses:
- React
- Typescript
- [Testing Library](https://testing-library.com/)
- Jest
- [Jest Chain](https://github.com/mattphillips/jest-chain)
- [Jest Extended](https://github.com/jest-community/jest-extended)

## Usage

Execute tests
```bash
yarn test
```

Execute tests with coverage
```bash
yarn test:ci
```
