import React, { PropsWithChildren } from 'react';
import './title.styles.css';

export type TitleProps = PropsWithChildren<unknown> & {
  /**
   * Indicates if title should be in bold text
   */
  bold?: boolean;
};

function Title({ children, bold }: TitleProps) {
  return <div className={bold ? 'bold' : ''}>{children}</div>;
}

export default Title;
