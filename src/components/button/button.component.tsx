import React, { PropsWithChildren } from 'react';
import Title from '../title/title.component';

export type ButtonProps = PropsWithChildren<unknown> & {
  /**
   * Indicates if button is disabled
   */
  disabled?: boolean;
  /**
   * Click event
   */
  onClick?: () => void;
};

function Button({ children, disabled, onClick }: ButtonProps) {
  const clickHandler = () => {
    if (!disabled && onClick) {
      onClick();
    }
  };

  // Disabled property should be added to button, but we remove it for test purposes
  return <button onClick={clickHandler}>
    <Title>This is a button</Title>
    {children}
  </button>;
}

export default Button;
